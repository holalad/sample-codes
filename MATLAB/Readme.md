#knn classifier

The directory knn comprises of the following scripts:

1. parser.m : This function is used to parse the text file into a feature matrix.
    * Input :
         - a string containing the path to the text file.
    * Ouput :
         - a feature matrix of size N X d, where N is the total number of instances and d the total number of features.


2. category2binary.m : The function will convert the categorical features from the training data into binary features. If there are K categories in a feature, then this function will create a new real-valued feature vector of K-dimension for that feature. In which, if the ith value is 1 then it
    means that its the ith category.
    * Input :
        - feature matrix N X d
        
    * Output 
        - binary_feature_vector of size N X (K1+K2+...+Kd), where d is the total number of features and K1, K2 ...Kd are the number of cateogries in feature 1, 2 ...d.


3. knn_loocv.m : The function will perform leave-one out cross validation scheme on the training dataset and selects the best k based on the k that gives the highest accuracy.
    * Inputs:   
          - train_data: N X d matrix, each row as a sample and each column as a feature.
          - train_label: N X 1 vector, each row as a label.
          - k: number of nearest neighbors, k can also be a vector of possible k's.

    * Output:
          - best_k : returns the best k if a set of k values are passed.


4. knn_classifier.m : The function uses the training dataset, best_k obtained from the knn_loocv and classifies all the test instances.
    * Inputs:   
          - train_data: N X d matrix, each row as a sample and each column as a feature.
          - test_data: M X d matrix, each row as a sampe and each column as a feature.
          - k: best_k

    * Output:
          - classified_class : returns the classified labels


5. main_knn.m : This is the main function which calls all the above functions in the form of command-line call from the command prompt.
    * Inputs:
          - argv[1] - train_file, train file is a N X (d+1) matrix, where is the number of instances and d the number of feature. 1 i.e. the last column is the class labels.

          - argv[2] - test_file, similar tot he train file, it is a M X d matrix, where M is the number of test instances and d is the number of features.

          - argv[3] - k, is an optional parameter. It can either be an array of k's or a signle value.
          
          - argv[4] - 'categorical', is an option parameter. User will use this if there are categorical features in the analysis by using the string 'categorical'. If the features are discreet values then this option can be skipped.
     
     * Calling the main_knn :
         - [ train_file test_file k 'categorical'] - all four parameters are specified
         - [  train_file test_file k ] - non-categorical features
         - [ train_file test_file 'categorical'] - categorical features but consider default k values
         - [ train_file test_file ] - non-categorical and consider default k values


#Usage

Clone the directory to your local and run main_knn with the appropriate parameters.

Run the main_knn function with the aforementioned settings and test the results. 