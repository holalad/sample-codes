function best_k = knn_loocv(train_data, train_label, k)
% k-nearest neighbor classifier
% Input:
%  train_data: N*D matrix, each row as a sample and each column as a
%  feature
%  train_label: N*1 vector, each row as a label
%  k: number of nearest neighbors, k can also be a vector of possible k's.

%Output:
%best_k : returns the best k if a set of k values are passed.

%The knn_loocv function uses Leave one out cross-validation scheme to get
%the best k
%%
fprintf('Leave-one-out cross validation commences \n');
[N,~] = size(train_data);

%%leave one out cross-validation
for j = 1: length(k) 
    identified_class ={};
    for i = 1:N
        %calculate the distance of instance with all the other instances
        instance = train_data(i,:);
        new_data = repmat(instance, [N,1]);
        
        %calculating the eucladian distance
        diff = train_data - new_data;
        squared_diff = diff.*diff;
        squared_sum = sum(squared_diff,2);
        distances = sqrt(squared_sum);
        
        %setting distance(i) = a max value so that it is not considered when
        %looking for the minimum distance
        distances(i) = max(distances);
 
        %sort the distances in ascending order
        [~,index] = sort(distances);
        
        %select the k-nearest neighbors
        nearest_neighbors = train_label(index(1:k(j)));
        
        %find the occurence of each neighbor class and get the maximum
        %ocurring neighbor class
        classes = unique( nearest_neighbors ,'stable');
        occurence = cellfun(@(x) sum(ismember(nearest_neighbors,x)),classes,'un',0);
        [~, max_index] =max([occurence{1:end}]);
        identified_class(i) = classes(max_index);
    end
    %compare the classified classes with the actual classes
    correct_detected(j) = sum(cellfun(@strcmp, train_label, identified_class'));
    fprintf('Accuracy for k = %d is %f \n',k(j), correct_detected(j)*100/N);
    
end 
%select the k with the highest accuracy
[~,index_k] = max(correct_detected);
best_k = k(index_k);
fprintf('Leave-one-out cross validation ends and the best k is %d \n', best_k);
end
