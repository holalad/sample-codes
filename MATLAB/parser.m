function feature_matrix = parser(filename)
%% this script is used to parse the text file into a feature matrix.
%feature_matrix : Nxd matrix.

    %%
    %feature matrix initialization
    feature_matrix = [];
    
    %Read the file line by line without the newline character
    fileID = fopen(filename);
    line = fgetl(fileID);
    while ischar(line)
        split_line = parse_line(line);
        feature_matrix = [feature_matrix ; split_line];
        line = fgetl(fileID);
   
    end
end

%% split the line using the comma delimiter
function parsed_line = parse_line(line)
    parsed_line = strsplit(line, ',');
end