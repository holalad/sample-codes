
function main_knn(varargin )

%%This function is written so that the user can input arguments to the
%function from the command prompt.

%The user will send the following parameters in the following sequence
%   argv[1] - train_file, train file is a N X (d+1) matrix, where is the
%             number of instances and d the number of feature. 1 i.e. 
%             the last column is the class labels.

%   argv[2] - test_file, similar tot he train file, it is a M X d matrix,
%             where M is the number of test instances and d is the number 
%             of features.

%   argv[3] - k, is an optional parameter. It can either be an array of k's
%             or a signle value.
%   argv[4] - 'categorical', is an option parameter. User will use this if
%             there are categorical features in the analysis by using the 
%             string 'categorical'. If the features are discreet values 
%             then this option can be skipped.


%Eg: ['car_train.data' 'car_test.data' [1:100] 'categorical'].

%%
    %check for at least two parameters: the train and the test file
    if nargin < 2
        error('main_knn atleast needs two arguments',...
            '[train_file test_file]');
    else
        train_file = varargin{1};
        test_file = varargin{2};
        
     %check for more than 4 parameters   
        if nargin > 4
            error('main_knn takes a maximum of 4 arguments');
        
        %check if the 4 parameters are passed
        elseif nargin == 4
            k = varargin{3};
            
            %check if the string is mentioned correctly, if so then
            %consider that the features are categorical
            if strcmp (varargin{4},'categorical')
                flag_category = 1;
            else
                error('The argument to specify that there are',...
                    'categorical variables in the data is a string:',...
                    '         "categorical"           ');
            end
            
        %check for categorical but default usage of 'k'    
        elseif nargin == 3 
            if ischar(varargin{3})
                if strcmp (varargin{3},'categorical')
                    flag_category = 1;
                    k = 1:2:15;       
                else
                    k = varargin{3};
                    flag_category = 0; 
                end
        
        % default k and non-categorical features        
        elseif nargin == 2
            k = 1:2:15;
            flag_category = 0;
            end
        end
    end
    
    %%parses the train and the test files
    train_data_parsed = parser(train_file);
    test_data_parsed = parser(test_file);
    
    %%checks for categorical flag and uses categorical to binary function
    %if categorical features are present 
    if flag_category
        train_data = category2binary(train_data_parsed(:,1:end-1));
        test_data = category2binary(test_data_parsed(:,1:end-1));
    else
        train_data = train_data_parsed(:,1:end-1);
        test_data = test_data_parsed(:,1:end-1);
    end
    
    %training labels are the last columns in the files
    train_label = train_data_parsed(:,end);
    test_label = test_data_parsed(:,end);
    
    %find the k using knn_loocv
    best_k = knn_loocv(train_data, train_label,k);
    
    %use the k to call the knn_classifier
    classes = knn_classifier (train_data, train_label, test_data, k);
    
    %calculate the accuracy by comparing the classified with actual labels
    correct_detected = sum(cellfun(@strcmp, test_label, classes'));
    fprintf('\n the total accuracy on the test data set is %f\n',correct_detected*100/size(test_data,1));
end    