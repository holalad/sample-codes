function identified_class = knn_classifier(train_data, train_label, test_data, k)
%% k-nearest neighbor classifier
% Input:
%  train_data: N*D matrix, each row as a sample and each column as a
%  feature
%  train_label: N*1 vector, each row as a label
%  test_data : M*D matrix, each row as a sample and each column as a
%  feature
%  k: number of nearest neighbors, k can also be a vector of possible k's.

%Output:
%classified_class : returns the classified class of each test instance
%%
    [N_test,~] = size(test_data);
    [N_train,~] = size(train_data);
    identified_class ={};
    
        for i = 1:N_test
            
            %calculate the distance of instance with all the other instances
            instance = test_data(i,:);
            new_data = repmat(instance, [N_train,1]);
            
            %calculating the eucladian distance
            diff = train_data - new_data;
            squared_diff = diff.*diff;
            squared_sum = sum(squared_diff,2);
            distances = sqrt(squared_sum);
            
            %sort the distances in ascending order
            [~,index] = sort(distances);
            
            %select the k-nearest neighbors
            nearest_neighbors = train_label(index(1:k));
            
            %find the occurence of each neighbor class and get the maximum
            %ocurring neighbor class
            classes = unique( nearest_neighbors ,'stable');
            occurence = cellfun(@(x) sum(ismember(nearest_neighbors,x)),classes,'un',0);
            [~, max_index] =max([occurence{1:end}]);

            identified_class(i) = classes(max_index);
        end
    end 
