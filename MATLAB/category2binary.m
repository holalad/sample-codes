function binary_feature_vector =  category2binary(feature_matrix)
    %% 
    %This function will convert the categorical features from the training
    %data into binary features. If there are K categories in a feature,
    %then this function will create a new real-valued feature vector of
    %K-dimension for that feature. In which, if the ith value is 1 then it
    %means that its the ith category.
    %dimension of binary_feature_vector : K1+K2+...+Kd, where d is the
    %total number of features and K1, K2 ...Kd are the number of cateogries
    %in feature 1, 2 ...d.
    
    %%
    [N,d] =size(feature_matrix);
    binary_feature_vector = [];
    
    %%
    for i = 1:d
        %get the unique categories in a feature
        unique_categories = sort(unique(feature_matrix(:,i)));
        
        %get the numerical value for the categorical label
        [~,index] = ismember(feature_matrix(:,i),unique_categories);
        new_feature_matrix = zeros(N,length(unique_categories));
        
        % convert the sub indices to linear indices
        rows = 1:N;
        cols = index';
        lin_ind = sub2ind(size(new_feature_matrix), rows, cols);
        
        % use the linear indices to change all the respective indices
        % to one and have the other elements in row equal to zero
        new_feature_matrix(lin_ind) = 1;
        binary_feature_vector =[ binary_feature_vector new_feature_matrix];
        
        clear new_feature
        clear unique_categories;
    end
end