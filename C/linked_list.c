
#include "linked_list.h"
/* insert a node functions*/
	
	//	adds node as the first element - O(1)
	void prepend_node ( node** head, int value ){
		//create a node
		node* new_node = malloc(sizeof(node));
		// store the value in the node
		new_node ->value = value;
		// point the new node's next pointer to the head of the list
		new_node ->next = *head;
		//change the head of the list to the new_node
		*head = new_node;
	}

	// 	adds the new node as the last element - O(n) 
	void append_node ( node** head, int value){
		// create a node
		node* new_node = malloc(sizeof(node));
		// store the value in the node
		new_node ->value = value;
		//point the new_node's next pointer to NULL as it will be the last element
		new_node ->next =NULL;
		//create a temp node to traverse the list
		node* temp =  *head;
		// if the list is empty
		if(*head == NULL){
			*head = new_node;
		}
		// traverese till the last node and append then new_node
		while(temp ->next != NULL){
			temp = temp ->next;
		}
		temp ->next = new_node;
	}
	// 	adds the new node in the nth position
	void insert_node (node ** head, int value, int position){
		
		if(position == 0 ){
			prepend_node(&(*head), value);
			return;
		}
		else{

			// create a node
			node* new_node = malloc(sizeof(node));
			// store the value in the node
			new_node ->value = value;
			//point the new_node's next pointer to NULL as it will be the last element
			new_node ->next =NULL;
			// create a temp node to traverse the list
			node *temp = *head;
			//check if the position is the first element of the list

			//get the lenght of the list and check if the position entered exceeds the length of the list
			int n = length_list(temp);
	
			if(position > n){
				printf("The lenght of the list is %d, position exceeds the list size", n);
				return;
			}

			//enter the node at the give position (indexed from 0,1,2...)
			int count = 0;
			node* next = NULL;

			while(temp != NULL) {
				if(count == position - 1) {
					next = temp ->next;
					temp ->next = new_node;
					new_node ->next = next;
					break;
				}
				count++;
				temp = temp ->next;
			}

			return;
		}
	}

	/* delete node(s) function */
	

	void delete_first_node (node** head){
		if (*head == NULL)
			return;
		node* temp = *head;
		*head = temp ->next;
		free(temp);
	}

//	will delete the last node in the list
	void delete_last_node (node **head){
		// if the list is empty
		if(head == NULL)
			return;
		
		node* prev = *head;
		node* current = prev ->next;
		// if there is only one node in the list
		if(current == NULL){
			free(prev);
			*head = NULL;
			return;
		}
		// free the last node
		
		while (current ->next != NULL) {
			prev = current;
			current = current ->next;
			
		}
		prev ->next = NULL;
		free(current);

	}

// 	will delete the nth node in the list
	void delete_node (node** head, int position){
		
		if (*head == NULL)
			return;
		//if its the first node that has to be deleted
		if(position == 0){
			delete_first_node(&(*head));
			return;
		}

		// get the length of the list
		node *temp = *head;
		int n = length_list (temp);

		//assign a counter to check for the node at postion to be deleted
		int count =0;

		if(position > n){
			printf("The lenght of the list is %d, position exceeds the list size", n);
			return;
		}

		node *prev = *head;
		node *current = prev ->next;

		while (current->next != NULL){

			if( count == position - 1){
				prev ->next = current ->next;
				free(current);
				return;
			}
			count++;
			prev = current;
			current = current ->next;
		}
	}

// 	will delete the whole linked list
	void delete_list (node** head){
		node *prev = *head;
		node *current = prev ->next;

		while (current != NULL){
			free(prev);
			prev = current;
			current = current ->next;
		}
		*head = current;
	}

	// length of the linked list
	int length_list (node* head){
		int count = 0;
		while(head != NULL){
			head = head ->next;
			count++;
		}
		return count;
	}


	//print the linked list

	void print_list (node* head){
		if(head == NULL)
			printf("The list is empty\n");

		//traverese the list and print elements
		node* temp = head;

		while(temp != NULL){
			printf("%d\n",temp ->value);
			temp = temp ->next;
		}

	}