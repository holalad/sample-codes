//linked list header file which will comprise of the relative functions and structures 
//necessary to build the list.
	
#include <stdio.h>
#include <stdlib.h>


	/* data-type node */
	typedef struct node {
		int value;
		struct node* next;
	}node;
	
	/* insert a node functions*/
	
	//	adds node as the first element
	void prepend_node ( node** head, int value );

	// 	adds the new node as the last element
	void append_node ( node** head, int value);
	
	// 	adds the new node in the nth position
	void insert_node (node ** head, int value, int position);


	/* delete node */
	
	//	will delete the first node in the list
	void delete_first_node (node** head);

	//	will delete the last node in the list
	void delete_last_node (node** head);

	// 	will delete the nth node in the list
	void delete_node (node** head, int position);

	// 	will delete the whole linked list
	void delete_list (node** head);

	/* length of the node */

	// get the length of the list
	int length_list(node*head);

	//print the linked list
	void print_list( node* head);

