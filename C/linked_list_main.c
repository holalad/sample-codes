//linked list main


#include "linked_list.h"



int main(){
	node* current = NULL;

	print_list(current);
	prepend_node(&current,5);
	append_node(&current,10);
	insert_node (&current,7,1);
	insert_node(&current,2,0);
	insert_node(&current, 18, 4);
	print_list(current);
	printf("\nnew list\n" );
	delete_node(&current,3);
	insert_node(&current,1,0);
	print_list(current);

	delete_list(&current);
	print_list(current);
	return 0;
}